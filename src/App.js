import React from 'react'

import { Switch, Route } from 'react-router-dom'

import Login from './containers/Login'
import Teachers from './containers/Teachers'

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={Login} />
      <Route exact path="/lists" component={Teachers} />

    </Switch>
  )
}

export default App

