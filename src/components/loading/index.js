import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'

// css, gif
import './loading.scss'

const Loading = () => {
    return(
		<div className="loading">
			<div className="container-loading">
				<div className="row-loading">
					<CircularProgress color="secondary" size={40} />
				</div>
				<div className="row-paragraph">
					<p className="title">Cargando...</p>
				</div>
			</div>
		</div>
	)
}


export default Loading