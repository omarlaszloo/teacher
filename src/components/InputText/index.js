import React from 'react'

//Material ui 
import MuiTextField from '@material-ui/core/TextField' 
import { fieldToTextField } from 'formik-material-ui' 

import './inputText.scss'
 
const InputText = props => { 
    
    const { 
        maxlength, 
        defaultValue, 
        tabindex,
        name
    } = props
    
    return ( 
        <MuiTextField
            className='input'
            {...fieldToTextField(props)} 
            defaultValue={defaultValue} 
            inputProps={{ maxLength:  maxlength }} 
            onChange={event => { 
                const { value } = event.target 
                props.form.setFieldValue( 
                props.field.name, 
                value ? value: '' 
                ) 
            }}
            
        /> 
    )
} 
 
export default InputText 