
import {

    ADD_TEACHER,
    DELETE_TEACHER,
    SHOW_MODAL,
    HIDE_MODAL
    
} from './constansts'

const initialState = {
    teachers: [],
    showModal: false,
    hideModal: false
}

export default function (state = initialState, action) {
    switch(action.type) {


        case ADD_TEACHER:
            return {
                ...state,
                teachers: action.payload
            }

        case SHOW_MODAL:
            return {
                ...state,
                showModal: true,
                hideModal: false
            }

        case HIDE_MODAL:
            return {
                ...state,
                showModal: false,
                hideModal: true
            }
        
        default:
            return state
    }
}
