import callApi from '../../utils/apiCaller'


import {
    ADD_TEACHER,
    DELETE_TEACHER,
    SHOW_MODAL,
    HIDE_MODAL
    
} from './constansts'

export const addTeacher = data => ({ type: ADD_TEACHER, payload: data })

export const deleteTeacher = data => ({ type: DELETE_TEACHER, payload: data })

export const showModal = data => ({ type: SHOW_MODAL, payload: data })

export const hideModal = data => ({ type: HIDE_MODAL, payload: data })

export const actionsAddTeacher = data => {
    return async dispatch => {
        
        let p1 = await callApi(`users/${data.idUser}`, 'POST', data)

        if (p1.code == 200) {

            dispatch(addTeacher(data))
            dispatch(hideModal())
        }


    }
}

export const actionsDeleteTeacher = data => {
    return async dispatch => {
        
        let p1 = await callApi(`users/${data.idUser}`, 'DELETE', data)

        if (p1.code == 200) {

            dispatch(deleteTeacher(data))
            dispatch(hideModal())
        }


    }
}

export const actionShowModal = () => {
    return dispatch => {
        dispatch(showModal())
    }
}

export const actionsHideModal = () => {
    return dispatch => {
        dispatch(hideModal())
    }
}

