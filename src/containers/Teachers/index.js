import React from 'react'
import TableComponent from '../../components/TableComponent'

import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'

import { actionsAddTeacher, actionShowModal, actionsHideModal } from './actions'

import ModalComponent from '../../components/Modal'

import { useDispatch, useSelector } from 'react-redux'

import InputText from '../../components/InputText'

import { Formik, Form, Field } from 'formik' 

import './teachers.scss'

const Teachers = () => {

    const dispatch = useDispatch()
    const add = async idPolicy => await dispatch(actionsAddTeacher(idPolicy))
    const show = () => dispatch(actionShowModal())
    const hide = () => dispatch(actionsHideModal())
    
    const submit = () => {
        add()
    }
    
    const state = useSelector(state => state)

    const handlerShowModal = () => {
        show()
    }

    const validations = {
        name: '',
        lastName: ''
    }


    return (
        <div className="container-lists">

            <ModalComponent
                sizeModal = "sm"
                title = "Asignar Maestros"
                btnTitle1 = "Guardar"
                next = {() => submit()}
                showModal = {state.Teachers.showModal}
                hideModal={hide}>
                
                <Formik
					initialValues={validations}
					validate={values => {
						const errors = {}
						if (!values.identification) {
							errors.identification = "*Campo requerido"
						}

						return errors
					}}
					onSubmit= {values => {
						console.log('values>>>>>', values)
						
					}}
					render = {( { submitMain }) => (
					
					<Form className="add-seller">
						<div className="field">    
                            <Field
                                disabled={false}
                                type="text"
                                name="name"
                                component={InputText}
                                label="Nombre"
                            />
                        </div>
                        <div className="field">    
                            <Field
                                disabled={false}
                                type="text"
                                name="lasteName"
                                component={InputText}
                                label="Apellidos"
                            />
                        </div>
                        <div className="field">    
                            <Field
                                disabled={false}
                                type="text"
                                name="edad"
                                component={InputText}
                                label="edad"
                            />
                        </div>
                        <div className="field">    
                            <Field
                                disabled={false}
                                type="text"
                                name="materia"
                                component={InputText}
                                label="Materia asig"
                            />
                        </div>
					</Form>
					)}
				/>
            </ModalComponent>
            <div className="container-table">
                <TableComponent />
            
                <div className="container-add">
                    <div className="row">
                        <Fab color="primary" aria-label="add" onClick={() => handlerShowModal()}>
                            <AddIcon />
                        </Fab>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Teachers