export const ADD_TEACHER = 'ADD_TEACHER'

export const DELETE_TEACHER = 'DELETE_TEACHER'

export const SHOW_MODAL = 'SHOW_MODAL'

export const HIDE_MODAL = 'HIDE_MODAL'
