/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import Teachers from './containers/Teachers/reducers'

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
const createRootReducer = history => combineReducers({
  Teachers,
  router: connectRouter(history)
})

export default createRootReducer
